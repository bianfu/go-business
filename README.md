# go-business

SDKs for interaction between apulis platform modules

## How to use it

1. 添加submodule

```go
git submodule add git@apulis-gitlab.apulis.cn:sdk/go-business.git deps/go-business
```

2. go.mod replace

```go
require (
	github.com/apulis/go-business v0.0.0
)

replace github.com/apulis/go-business v0.0.0 => ./deps/go-business
```

3. 如果本地没有deps，执行命令获取

```sh
git submodule init
git submodule update
```
