# ImgProxy SDK

图片服务器sdk

## 创建客户端

### 客户端配置

1. 配置文件结构体

* 结构体位于`pkg/imgproxy/config.go/ImgProxyConfig`
* 结构体字段说明:
    - `Enctypted`：是否使用加密签名，尽量选择是
    - `Key`：签名key，字符串, hex加密
    - `Salt`：签名salt, 字符串，hex加密
    - `StorageType`: 原图存储方式，目前支持以下几种
        * `local`：文件系统(本地磁盘或挂载到本地的nfs/ceph等)
        * `s3`: s3接口的对象存储
        * `gs`：google对象存储
        * `abs`：微软对象存储
        * `http`、`https`：图片url，任意可访问的网站
    - `RealUrlPrefix`: 真实的url前缀，拼好后直接给前端显示图片使用
* 生成结构体用例
    ```go
    config := &imgproxy.ImgProxyConfig{
        Enctypted:     true,
        Key:           "6170756C69735F6B6579",
        Salt:          "6170756C69735F73616C74",
        StorageType:   "local",
        RealUrlPrefix: "http://localhost/test",
    }
    ```

2. 生成客户端实例

```go
client, err := imgproxy.CreateImgProxyClient(config)
```

### 定义操作

定义操作可指定图片行为，已实现函数：
* Resize: 图片大小更改
* ... TODO

#### Resize

```go
client.NewOperation().Resize(imgproxy.ResizeTypeFit, 300, 500, true)
```

#### TODO

### 设置代理图片

添加图片位置，添加在相应存储的绝对路径即可
* local: 填入相对路径(相对代理文件夹位置，以"/"开头)
* s3：bucket/图片名
* ...

```go
client.NewOperation().Resize(imgproxy.ResizeTypeFit, 300, 500, true).AddFilePath("/1.jpg")
```

### 生成真实url

确认完成以上步骤，调用GenerateURL()方法，生成url

```go
client.NewOperation().Resize(imgproxy.ResizeTypeFit, 300, 500, true).AddFilePath("/1.jpg")
```
