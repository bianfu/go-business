# JWT Gin 中间件



### 示例说明

```go
import (
	"github.com/apulis/go-business/pkg/jwt"
	"github.com/gin-gonic/gin"
	"testing"
)

func TestJwtMiddleware(t *testing.T) {
	authn := jwt.NewJwtAuthN(
		jwt.SigningAlgorithm("HS256"),
		jwt.SecretKey([]byte("jwt secret key")),
		jwt.PublicKey("publicKey")
	)

	e := gin.Default()
	e.Use(authn.Middleware())
}

```

说明：

```jwt.SigningAlgorithm```  设置签名算法，HS256, HS384, HS512, RS256, RS384 or RS512， 其中RS* 是非对称加密；

如果签名算法是HS*，设置```jwt.SecretKey```，否则需要设置```jwt.PublicKey```


### Context信息
中间件会在gin.Context里面保存用户的信息，例如userId, userName等，具体获得方法如下:
```go
# c为 gin.Context
jwt.UserGroupId(c)
jwt.UserName(c)
jwt.UserId(c)
jwt.OrgId(c)
```

### 错误码
直接返回IAM模块的错误码