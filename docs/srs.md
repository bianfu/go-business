# Srs SDK

流媒体服务器SRS sdk

## 创建客户端

### 客户端配置

1. 配置文件结构体

* 结构体位于`pkg/srs/config.go/SrsConfig`
* 结构体字段说明：
    - `RtmpPushHost`: 推流rtmp的host
    - `RtmpPushPort`: 推流rtmp的端口号，默认1935
    - `RealUrlPrefix`:  真实的url前缀，与后缀拼接成文件地址

2. 生成客户端实例

```go
config := &srs.SrsConfig{
    RtmpPushHost:  "srs.default",
    RtmpPushPort:  1935,
    RealUrlPrefix: "http://192.168.1.185:18080",
}
client := srs.CreateSrsClient(config)
```

### 功能

1. 生成直播流地址
2. 生成视频点播地址
