/* ******************************************************************************
* 2019 - present Contributed by Apulis Technology (Shenzhen) Co. LTD
*
* This program and the accompanying materials are made available under the
* terms of the MIT License, which is available at
* https://www.opensource.org/licenses/MIT
*
* See the NOTICE file distributed with this work for additional
* information regarding copyright ownership.
* Unless required by applicable law or agreed to in writing, software
* distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
* WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
* License for the specific language governing permissions and limitations
* under the License.
*
* SPDX-License-Identifier: MIT
***************************************************************************** */
package imgproxy

import (
	"fmt"
	"strings"
)

type ImgProxyConfig struct {
	Enctypted bool
	Key       string
	Salt      string

	StorageType   string
	RealUrlPrefix string
}

const storageTypeLocal = "local"
const storageTypeS3 = "s3"
const storageTypeGS = "gs"
const storageTypeABS = "abs"
const storageTypeExternalHttp = "http"
const storageTypeExternalHttps = "https"

var storageTypes = map[string]bool{
	storageTypeLocal:         true,
	storageTypeS3:            true,
	storageTypeGS:            true,
	storageTypeABS:           true,
	storageTypeExternalHttp:  true,
	storageTypeExternalHttps: true,
}

func (imgProxyConfig *ImgProxyConfig) CheckImgProxyConfigValid() error {
	if !storageTypes[imgProxyConfig.StorageType] {
		return fmt.Errorf("Storage type %s not supported ...", imgProxyConfig.StorageType)
	}

	if !strings.HasPrefix(imgProxyConfig.RealUrlPrefix, "http") {
		return fmt.Errorf("Real url prefix not valid: %s", imgProxyConfig.RealUrlPrefix)
	}

	imgProxyConfig.RealUrlPrefix = strings.TrimSuffix(imgProxyConfig.RealUrlPrefix, "/")
	return nil
}
