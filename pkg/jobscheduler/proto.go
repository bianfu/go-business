/* ******************************************************************************
* 2019 - present Contributed by Apulis Technology (Shenzhen) Co. LTD
*
* This program and the accompanying materials are made available under the
* terms of the MIT License, which is available at
* https://www.opensource.org/licenses/MIT
*
* See the NOTICE file distributed with this work for additional
* information regarding copyright ownership.
* Unless required by applicable law or agreed to in writing, software
* distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
* WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
* License for the specific language governing permissions and limitations
* under the License.
*
* SPDX-License-Identifier: MIT
***************************************************************************** */
package jobscheduler

// 1
// create job request
type CreateJobReq struct {
	Job
}

// create job response
type CreateJobRsp struct {
	JobId    string   `json:"jobId"`
	JobState JobState `json:"jobState"`
}

// 2
// get job response
type GetJobRsp struct {
	*Job
	JobStatus string
	KubeMsg   string
}

// 4
// get log request
type GetLogReq struct {
	PageNum int `json:"pageNum" form:"pageNum"`
}

// get logs
type GetLogRsp struct {
	Items   []string `json:"items"`
	Total   int      `json:"total"`
	HasNext bool     `json:"hasNext"`
}

// resource quota
type ResourceQuota struct {
	Request ResourceData `json:"request"`
	Limit   ResourceData `json:"limit"`
}

type ResourceData struct {
	// e.g.
	// memory unit：Ei，Pi，Ti，Gi，Mi，Ki
	// cpu unit： 1, 100m（1/10 CPU）
	CPU    string `json:"cpu"`
	Memory string `json:"memory"`
	Device Device `json:"device"`
}

func (r *ResourceData) Empty() bool {

	if len(r.CPU) == 0 && len(r.Memory) == 0 && r.Device.EmptyDevice() {
		return true
	}

	return false
}

// device info
type Device struct {

	// e.g.
	//	"deviceType":"npu.huawei.com/NPU",
	//	"deviceNum":"1",
	//	"series":"a910",
	//	"computeType":"huawei_npu"

	// for kubernetes resources request
	DeviceType string `json:"deviceType"`
	DeviceNum  string `json:"deviceNum"`

	// for node selector
	Series      string `json:"series"`
	ComputeType string `json:"computeType"`
}

func (d *Device) EmptyDevice() bool {

	if len(d.DeviceType) == 0 || len(d.DeviceNum) == 0 {
		return true
	}

	return false
}

func (d *Device) GetSeries() string {
	return d.Series
}

func (d *Device) GetComputeType() string {
	return d.ComputeType
}

//func (c *ComputeResUsageItem) SetType()  {
//	c.Type = resType
//}
//
//func (c *ComputeResIndex) SetDevice(series, computeType string)  {
//
//	c.Series = series
//	c.ComputeType = computeType
//	computeType = strings.ToLower(computeType)
//
//	if strings.Contains(computeType, "gpu") {
//		c.SetType(COMPUTE_RES_TYPE_GPU)
//
//	} else if strings.Contains(computeType, "npu") {
//
//		c.SetType(COMPUTE_RES_TYPE_NPU)
//	}
//}

// 6
// create KFServing inference service
// JobBase.resType == RESOURCE_TYPE_INFERENCE_SERVICE
// POST api/v1/inference-jobs
type CreateInferenceJobReq struct {
	InferenceJob
}

// 7
// delete KFServing inference service
// DELETE api/v1/inference-jobs/:id

// 8
// get job pods
// GET api/v1/misc/get-pods?jobId=
type GetJobPodsReq struct {
	JobId string `json:"jobId" form:"jobId"`
}

type GetJobPodsRsp struct {
	Pods []PodItem `json:"pods"`
}

// 9
// get job tips
// GET api/v1/misc/job-tips?jobId=
type GetJobTipsReq struct {
	JobId string `json:"jobId" form:"jobId"`
}

type GetJobTipsRsp struct {
	Tips string `json:"tips"`
}
