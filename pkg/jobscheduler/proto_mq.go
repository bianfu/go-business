/* ******************************************************************************
* 2019 - present Contributed by Apulis Technology (Shenzhen) Co. LTD
*
* This program and the accompanying materials are made available under the
* terms of the MIT License, which is available at
* https://www.opensource.org/licenses/MIT
*
* See the NOTICE file distributed with this work for additional
* information regarding copyright ownership.
* Unless required by applicable law or agreed to in writing, software
* distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
* WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
* License for the specific language governing permissions and limitations
* under the License.
*
* SPDX-License-Identifier: MIT
***************************************************************************** */
package jobscheduler

type MsgType int

const (
	MSG_TYPE_NONE           MsgType = iota
	MSG_TYPE_JOB_UPDATE     MsgType = 1
	MSG_TYPE_SERVICE_UPDATE MsgType = 2
)

// 3
// message queue protocol
type JobMsg struct {
	JobId    string
	JobState JobState

	MsgType MsgType `json:"msgType"`
	Body    string  `json:"body"`
}

func NewJobMsg(jobId string, msgType MsgType, body interface{}) *JobMsg {
	return &JobMsg{
		JobId:   jobId,
		MsgType: msgType,
		Body:    MustMarshalString(body),
	}
}

func NewJobStateMsg(jobId string, msgType MsgType, jobState JobState) *JobMsg {
	return &JobMsg{
		JobId:    jobId,
		MsgType:  msgType,
		JobState: jobState,
	}
}

func (j *JobMsg) GetBody() string {
	return j.Body
}

func (j *JobMsg) SetBody(body string) {
	j.Body = body
}

func (j *JobMsg) GetJobId() string {
	return j.JobId
}

func (j *JobMsg) SetJobId(jobId string) {
	j.JobId = jobId
}

func (j *JobMsg) GetMsgType() MsgType {
	return j.MsgType
}

func (j *JobMsg) SetMsgType(msgType MsgType) {
	j.MsgType = msgType
}

type ServiceState struct {
	Name     string `json:"name"`
	JobId    string `json:"jobId"`
	NodePort int32  `json:"nodePort"`
}

func NewServiceState(name, jobId string, nodePort int32) *ServiceState {
	return &ServiceState{
		Name:     name,
		JobId:    jobId,
		NodePort: nodePort,
	}
}

func (s *ServiceState) GetName() string {
	return s.Name
}

func (s *ServiceState) GetJobId() string {
	return s.JobId
}

func (s *ServiceState) GetNodePort() int32 {
	return s.NodePort
}
