/* ******************************************************************************
* 2019 - present Contributed by Apulis Technology (Shenzhen) Co. LTD
*
* This program and the accompanying materials are made available under the
* terms of the MIT License, which is available at
* https://www.opensource.org/licenses/MIT
*
* See the NOTICE file distributed with this work for additional
* information regarding copyright ownership.
* Unless required by applicable law or agreed to in writing, software
* distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
* WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
* License for the specific language governing permissions and limitations
* under the License.
*
* SPDX-License-Identifier: MIT
***************************************************************************** */
package jobscheduler

type PodItem struct {
	PodName           string            `json:"podName"`
	ContainerStatuses []ContainerStatus `json:"containerStatuses"`
	Labels            map[string]string `json:"labels"`
}

type ContainerStatus struct {
	Name        string `json:"name"`
	Image       string `json:"image"`
	ContainerID string `json:"containerID"`
}

type Toleration struct {
	Key      string `json:"key"`
	Operator string `json:"operator"`
	Value    string `json:"value"`
	Effect   string `json:"effect"`
}

func NewToleration() *Toleration {
	return &Toleration{}
}

func (t *Toleration) SetKey(key string) *Toleration {
	t.Key = key
	return t
}

func (t *Toleration) GetKey() string {
	return t.Key
}

func (t *Toleration) SetValue(value string) *Toleration {
	t.Value = value
	return t
}

func (t *Toleration) GetValue() string {
	return t.Value
}

func (t *Toleration) SetOperatorExist() *Toleration {
	t.Operator = "Exists"
	return t
}

func (t *Toleration) SetOperatorEqual() *Toleration {
	t.Operator = "Equal"
	return t
}

func (t *Toleration) GetOperator() string {
	return t.Operator
}

func (t *Toleration) SetTaintEffectNoSchedule() *Toleration {
	t.Effect = "NoSchedule"
	return t
}

func (t *Toleration) SetTaintEffectPreferNoSchedule() *Toleration {
	t.Effect = "PreferNoSchedule"
	return t
}

func (t *Toleration) SetTaintEffectNoExecute() *Toleration {
	t.Effect = "NoExecute"
	return t
}

func (t *Toleration) GetEffect() string {
	return t.Effect
}
