/* ******************************************************************************
* 2019 - present Contributed by Apulis Technology (Shenzhen) Co. LTD
*
* This program and the accompanying materials are made available under the
* terms of the MIT License, which is available at
* https://www.opensource.org/licenses/MIT
*
* See the NOTICE file distributed with this work for additional
* information regarding copyright ownership.
* Unless required by applicable law or agreed to in writing, software
* distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
* WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
* License for the specific language governing permissions and limitations
* under the License.
*
* SPDX-License-Identifier: MIT
***************************************************************************** */
package jobscheduler

import (
	"fmt"
	"strings"
)

const (
	RESOURCE_NAME_CPU = "cpu"
	RESOURCE_NAME_RAM = "ram"
)

// 5
// get target user group's resource usage
type GetResUsageReq struct {
	UserGroupId uint32 `json:"userGroupId" form:"userGroupId"`
}

type GetResUsageRsp struct {
	Items []ComputeResUsageItem `json:"items"`
}

type ComputeResUsageItem struct {
	ResourceName string `json:"ResourceName"`

	//	"series":"a910"
	//	"computeType":"huawei_npu"
	Series      string `json:"series"`
	ComputeType string `json:"computeType"`

	Used float32 `json:"used"`
}

func NewComputeResUsageItem(computeType, series string, used float32) *ComputeResUsageItem {

	item := &ComputeResUsageItem{
		Series:      series,
		ComputeType: computeType,
		Used:        used,
	}

	resName := fmt.Sprintf("%s_%s", strings.ToLower(computeType), strings.ToLower(series))
	item.SetResName(resName)

	return item
}

func NewCPUResUsageItem(used float32) *ComputeResUsageItem {

	item := &ComputeResUsageItem{
		Used: used,
	}

	resName := fmt.Sprintf("%s", RESOURCE_NAME_CPU)
	item.SetResName(resName)

	return item
}

func NewRAMResUsageItem(used float32) *ComputeResUsageItem {

	item := &ComputeResUsageItem{
		Used: used,
	}

	resName := fmt.Sprintf("%s", RESOURCE_NAME_RAM)
	item.SetResName(resName)

	return item
}

func (c *ComputeResUsageItem) SetResName(name string) {
	c.ResourceName = strings.ToLower(name)
}

func (c *ComputeResUsageItem) IsRAM() bool {
	if c.ResourceName == RESOURCE_NAME_RAM {
		return true
	} else {
		return false
	}
}

func (c *ComputeResUsageItem) IsCPU() bool {
	if c.ResourceName == RESOURCE_NAME_CPU {
		return true
	} else {
		return false
	}
}

func (c *ComputeResUsageItem) GetResName() string {
	return c.ResourceName
}

func GetResourceName(computeType, series string) string {
	return fmt.Sprintf("%s_%s", strings.ToLower(computeType), strings.ToLower(series))
}
