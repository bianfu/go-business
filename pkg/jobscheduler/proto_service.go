/* ******************************************************************************
* 2019 - present Contributed by Apulis Technology (Shenzhen) Co. LTD
*
* This program and the accompanying materials are made available under the
* terms of the MIT License, which is available at
* https://www.opensource.org/licenses/MIT
*
* See the NOTICE file distributed with this work for additional
* information regarding copyright ownership.
* Unless required by applicable law or agreed to in writing, software
* distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
* WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
* License for the specific language governing permissions and limitations
* under the License.
*
* SPDX-License-Identifier: MIT
***************************************************************************** */
package jobscheduler

// create service
// POST api/v1/services
type CreateServiceReq struct {
	JobId         string        `json:"jobId"`
	Namespace     string        `json:"namespace"`
	ContainerPort ContainerPort `json:"containerPort"`
}

type CreateServiceRsp struct {
}

// delete distributed application
// DELETE api/v1/services?jobId=&namespace=&serviceName=
type DeleteServiceReq struct {
	JobId       string `json:"jobId" form:"jobId"`
	Namespace   string `json:"namespace" form:"namespace"`
	ServiceName string `json:"serviceName" form:"serviceName"`
}
