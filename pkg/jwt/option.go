/* ******************************************************************************
* 2019 - present Contributed by Apulis Technology (Shenzhen) Co. LTD
*
* This program and the accompanying materials are made available under the
* terms of the MIT License, which is available at
* https://www.opensource.org/licenses/MIT
*
* See the NOTICE file distributed with this work for additional
* information regarding copyright ownership.
* Unless required by applicable law or agreed to in writing, software
* distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
* WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
* License for the specific language governing permissions and limitations
* under the License.
*
* SPDX-License-Identifier: MIT
***************************************************************************** */
package jwt

type Options struct {
	// signing algorithm - possible values are HS256, HS384, HS512, RS256, RS384 or RS512
	// Optional, default is HS256.
	SigningAlgorithm string

	// Secret key used for signing. Required.
	Key []byte

	// Public key file for asymmetric algorithms
	PubKeyFile string
}

type Option func(*Options)

func SigningAlgorithm(algorithm string) Option {
	return func(opts *Options) {
		opts.SigningAlgorithm = algorithm
	}
}

func SecretKey(secret []byte) Option {
	return func(opts *Options) {
		opts.Key = secret
	}
}

func PublicKey(pub string) Option {
	return func(opts *Options) {
		opts.PubKeyFile = pub
	}
}