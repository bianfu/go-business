/* ******************************************************************************
* 2019 - present Contributed by Apulis Technology (Shenzhen) Co. LTD
*
* This program and the accompanying materials are made available under the
* terms of the MIT License, which is available at
* https://www.opensource.org/licenses/MIT
*
* See the NOTICE file distributed with this work for additional
* information regarding copyright ownership.
* Unless required by applicable law or agreed to in writing, software
* distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
* WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
* License for the specific language governing permissions and limitations
* under the License.
*
* SPDX-License-Identifier: MIT
***************************************************************************** */
package logquery

import "strings"

func getContent(contentInclude, contentExclude []string) string {
	res := ""
	for _, v := range contentInclude {
		res += ("|=\"" + v + "\"")
	}

	for _, v := range contentExclude {
		res += ("!=\"" + v + "\"")
	}

	return res
}

func getStreamSelector(streamSelectorInclude, streamSelectorExclude map[string]string) string {
	if len(streamSelectorInclude) == 0 && len(streamSelectorExclude) == 0 {
	   return "{namespace=~\".+\"}"	
	}

	streamSelectorStr := "{"
	for k, v := range streamSelectorInclude {
		streamSelectorStr += (k + "=")
		streamSelectorStr += ("\"" + v + "\"")
		streamSelectorStr += ","
	}

	for k, v := range streamSelectorExclude {
		streamSelectorStr += (k + "!=")
		streamSelectorStr += ("\"" + v + "\"")
		streamSelectorStr += ","
	}

	// 去掉结尾的 ","
	if strings.HasSuffix(streamSelectorStr, ",") {
		streamSelectorStr = streamSelectorStr[:len(streamSelectorStr)-1]
	}
	streamSelectorStr += "}"

	return streamSelectorStr
}
