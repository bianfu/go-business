/* ******************************************************************************
* 2019 - present Contributed by Apulis Technology (Shenzhen) Co. LTD
*
* This program and the accompanying materials are made available under the
* terms of the MIT License, which is available at
* https://www.opensource.org/licenses/MIT
*
* See the NOTICE file distributed with this work for additional
* information regarding copyright ownership.
* Unless required by applicable law or agreed to in writing, software
* distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
* WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
* License for the specific language governing permissions and limitations
* under the License.
*
* SPDX-License-Identifier: MIT
***************************************************************************** */
package srs

import (
	"errors"
	"fmt"
	"strings"
)

type SrsClient struct {
	config *SrsConfig
}

type Urls struct {
	RtmpPushUrl string
	HttpFlvUrl  string
}

func CreateSrsClient(srsConfig *SrsConfig) *SrsClient {
	srsConfig.CheckSrsConfigValid()
	return &SrsClient{config: srsConfig}
}

func (srsClient *SrsClient) GenerateUrls(app, stream string) *Urls {
	rtmpUrl := fmt.Sprintf("rtmp://%s:%d/%s/%s", srsClient.config.RtmpPushHost, srsClient.config.RtmpPushPort, app, stream)
	httpFlvUrl := fmt.Sprintf("%s/%s/%s.flv", srsClient.config.RealUrlPrefix, app, stream)
	return &Urls{RtmpPushUrl: rtmpUrl, HttpFlvUrl: httpFlvUrl}
}

func (srsClient *SrsClient) GenerateVideoFileUrl(path string) (string, error) {
	if !strings.HasSuffix(path, ".mp4") {
		return "", errors.New("Video format not supported, only .mp4 supported")
	}
	path = strings.TrimPrefix(path, "/")
	url := fmt.Sprintf("%s/videos/%s", srsClient.config.RealUrlPrefix, path)
	return url, nil
}
