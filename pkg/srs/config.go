/* ******************************************************************************
* 2019 - present Contributed by Apulis Technology (Shenzhen) Co. LTD
*
* This program and the accompanying materials are made available under the
* terms of the MIT License, which is available at
* https://www.opensource.org/licenses/MIT
*
* See the NOTICE file distributed with this work for additional
* information regarding copyright ownership.
* Unless required by applicable law or agreed to in writing, software
* distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
* WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
* License for the specific language governing permissions and limitations
* under the License.
*
* SPDX-License-Identifier: MIT
***************************************************************************** */
package srs

import "strings"

const rtmpUrlPrefix = "rtmp://"

type SrsConfig struct {
	RtmpPushHost string
	RtmpPushPort int

	RealUrlPrefix string
}

func (srsConfig *SrsConfig) CheckSrsConfigValid() {
	if srsConfig.RtmpPushPort == 0 {
		srsConfig.RtmpPushPort = 1935
	}
	if strings.HasPrefix(srsConfig.RtmpPushHost, rtmpUrlPrefix) {
		srsConfig.RtmpPushHost = strings.TrimPrefix(srsConfig.RealUrlPrefix, rtmpUrlPrefix)
	}
	srsConfig.RealUrlPrefix = strings.TrimSuffix(srsConfig.RealUrlPrefix, "/")
}
