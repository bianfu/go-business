/* ******************************************************************************
* 2019 - present Contributed by Apulis Technology (Shenzhen) Co. LTD
*
* This program and the accompanying materials are made available under the
* terms of the MIT License, which is available at
* https://www.opensource.org/licenses/MIT
*
* See the NOTICE file distributed with this work for additional
* information regarding copyright ownership.
* Unless required by applicable law or agreed to in writing, software
* distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
* WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
* License for the specific language governing permissions and limitations
* under the License.
*
* SPDX-License-Identifier: MIT
***************************************************************************** */
package wsmsg

const MESSAGE_CENTER_TOPIC_NAME = "mct"

const (
	MCT_RECV_ALL   = 0 // broadcast all message
	MCT_RECV_USER  = 1 // user message
	MCT_RECV_GROUP = 2 // group message
	MCT_RECV_ORG   = 3 // org  message
)

const (
	MCT_CLASSIFY_SYSTEM    = 1 // system messages
	MCT_CLASSIFY_TRAIN     = 2 // train platform message
	MCT_CLASSIFY_ANNOT     = 3 // annotation system message
	MCT_CLASSIFY_INFERENCE = 4 // inference  system message
)

type ReqPublishMessage struct {
	CreatedAt    int64  `json:"createdAt"`
	Module       string `json:"module" `
	Subject      string `json:"subject"`
	Classify     int    `json:"classify"`
	Type         string `json:"type"`
	Receiver     uint64 `json:"receiver"`
	ReceiverType int    `json:"receiverType"`
	Deadline     int64  `json:"deadline"`
	Deconstruct  int    `json:"deconstruct"`
}
