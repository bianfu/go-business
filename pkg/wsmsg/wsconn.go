/* ******************************************************************************
* 2019 - present Contributed by Apulis Technology (Shenzhen) Co. LTD
*
* This program and the accompanying materials are made available under the
* terms of the MIT License, which is available at
* https://www.opensource.org/licenses/MIT
*
* See the NOTICE file distributed with this work for additional
* information regarding copyright ownership.
* Unless required by applicable law or agreed to in writing, software
* distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
* WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
* License for the specific language governing permissions and limitations
* under the License.
*
* SPDX-License-Identifier: MIT
***************************************************************************** */
package wsmsg

type PushType int

const (
	PushTypeUnicast      PushType = 1 //单播
	PushTypeBroadcast    PushType = 2 //广播
	PushTypeTopic        PushType = 3 //依据topic 广播
	PushTypeUserGroup    PushType = 4 //依据userGroupId发送 //todo
	PushTypeOrganization PushType = 5 //按照组织发送 //todo
)

const (
	CMD_TYPE_RUN_NEW    = "newRun"
	CMD_TYPE_RUN_DEL    = "delRun"
	CMD_TYPE_RUN_STATUS = "statusRun"

	CMD_TYPE_ENDPOINT = "endpoint"

	CMD_BELL_UPDATE_NUM = "bellUpdateNum"
)

// message queue
type PushMsgHeader struct {
	ReqId       string   `json:"reqId"`       //通知发送方的序列号,以此鉴别不同消息
	ModId       int      `json:"modId"`       //通知的发送方模块ID
	Cmd         string   `json:"cmd"`         //通知命令字
	PushType    PushType `json:"pushType"`    //消息发布类型  1.单播 2.广播 3.topic发送 4.给userGroup发送
	PushTargets []string `json:"pushTargets"` //当PushType=1 PushTargets为userId,PushType=2为所有用户都发送，，此时不需要PushTargets,当PushType=3,PushTargets为topic 当PushType=4 PushTargets userGroupId
}

type PushMsg struct {
	Header  PushMsgHeader `json:"header"`
	Payload interface{}   `json:"payload"`
	Scope   interface{}   `json:"scope"`
}
