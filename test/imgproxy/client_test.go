/* ******************************************************************************
* 2019 - present Contributed by Apulis Technology (Shenzhen) Co. LTD
*
* This program and the accompanying materials are made available under the
* terms of the MIT License, which is available at
* https://www.opensource.org/licenses/MIT
*
* See the NOTICE file distributed with this work for additional
* information regarding copyright ownership.
* Unless required by applicable law or agreed to in writing, software
* distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
* WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
* License for the specific language governing permissions and limitations
* under the License.
*
* SPDX-License-Identifier: MIT
***************************************************************************** */
package test

import (
	"fmt"
	"testing"

	"github.com/apulis/go-business/pkg/imgproxy"
)

func TestClient(t *testing.T) {
	config := &imgproxy.ImgProxyConfig{
		Enctypted:     true,
		Key:           "943b421c9eb07c830af81030552c86009268de4e532ba2ee2eab8247c6da0881",
		Salt:          "520f986b998545b4785e0defbc4f3c1203f22de2374a3d53cb7a7fe9fea309c5",
		StorageType:   "local",
		RealUrlPrefix: "http://localhost/test",
	}
	client, err := imgproxy.CreateImgProxyClient(config)
	if err != nil {
		t.Errorf("Create imgproxy client error: %s", err.Error())
	}
	fmt.Printf("Create imgproxy client success: %v\n", client)

	url := client.NewOperation().Resize(imgproxy.ResizeTypeFit, 300, 500, true).AddFilePath("/1.jpg").GenerateURL()
	fmt.Println(url)
	url = client.NewOperation().Resize(imgproxy.ResizeTypeFit, 200, 400, true).Quality(50).AddFilePath("/1.jpg").GenerateURL()
	fmt.Println(url)
}
