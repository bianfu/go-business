/* ******************************************************************************
* 2019 - present Contributed by Apulis Technology (Shenzhen) Co. LTD
*
* This program and the accompanying materials are made available under the
* terms of the MIT License, which is available at
* https://www.opensource.org/licenses/MIT
*
* See the NOTICE file distributed with this work for additional
* information regarding copyright ownership.
* Unless required by applicable law or agreed to in writing, software
* distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
* WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
* License for the specific language governing permissions and limitations
* under the License.
*
* SPDX-License-Identifier: MIT
***************************************************************************** */
package logquery

import (
	"fmt"
	"github.com/apulis/go-business/pkg/logquery"
	"testing"
	"time"
)

func TestLogQuery(t *testing.T) {
	m := make(map[string]string)
	m1 := make(map[string]string)

	now := time.Now()
	data, _ := logquery.QueryRange("192.168.3.172:3033", m, m1, nil, nil, 10, now.Add(-time.Hour), now, true)
	fmt.Println("data: ", data)
}


func TestLabels(t *testing.T) {
	now := time.Now()
	data, _ := logquery.Label("192.168.3.172:3033",false,now.Add(-time.Hour), now)
	fmt.Println(data)
}


func TestLabelValues(t *testing.T) {
	now := time.Now()
	data, _ := logquery.LabelValues("192.168.3.172:3033", false , "namespace", now.Add(-time.Hour), now)
	fmt.Println(data)
}
