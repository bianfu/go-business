/* ******************************************************************************
* 2019 - present Contributed by Apulis Technology (Shenzhen) Co. LTD
*
* This program and the accompanying materials are made available under the
* terms of the MIT License, which is available at
* https://www.opensource.org/licenses/MIT
*
* See the NOTICE file distributed with this work for additional
* information regarding copyright ownership.
* Unless required by applicable law or agreed to in writing, software
* distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
* WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
* License for the specific language governing permissions and limitations
* under the License.
*
* SPDX-License-Identifier: MIT
***************************************************************************** */
package test

import (
	"fmt"
	"testing"

	"github.com/apulis/go-business/pkg/srs"
)

func TestClient(t *testing.T) {
	config := &srs.SrsConfig{
		RtmpPushHost:  "srs.default",
		RtmpPushPort:  1935,
		RealUrlPrefix: "http://192.168.1.185:18080",
	}
	client := srs.CreateSrsClient(config)
	urls := client.GenerateUrls("test-app", "test-stream")
	fmt.Println(urls.RtmpPushUrl, urls.HttpFlvUrl)

	videoFileUrl, err := client.GenerateVideoFileUrl("dataset/videos/hello.mp4")
	fmt.Println(videoFileUrl, err)
}
